#!/bin/zsh

set -e

# Script for installing pycurl on MacOS
# The issue https://gist.github.com/vidakDK/de86d751751b355ed3b26d69ecdbdb99

# TLDR: for some reason python uses different curl versions
# during compiling `pip install pycurl` and in runtime `python -c 'import pycurl'`.
# This causes an error
#
# Traceback (most recent call last):
#   File "<string>", line 1, in <module>
# ImportError: pycurl: libcurl link-time version (7.64.1) is older than compile-time version (7.77.0)

# Here I download curl-7.64.1 and use it for `pycurl` compilation.
# Generally, it's better to set up new curl version via `brew install curl` and use it for both runtime and compile-time.
# But I didn't manage to change link-time version (7.64.1).

# Uninstall installed pycurl
pip uninstall -y pycurl

# We will use default curl, so remove brew installed one
#brew uninstall curl || true
#brew install openssl || brew update openssl

CURL_VERSION=$(curl -V | head -n 1 | awk '{ print $2 }')
PYCURL_VERSION=$(cat requirements.txt | grep 'pycurl==' | cut -d= -f3-)

# Downloading sources
mkdir -p .cache
wget "https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.bz2" -O  ".cache/curl-${CURL_VERSION}.tar.bz2"
tar -xf ".cache/curl-${CURL_VERSION}.tar.bz2" -C .cache

export PATH="$(pwd)/.cache/curl-${CURL_VERSION}/bin:$PATH"
export LDFLAGS="-L$(pwd)/.cache/curl-${CURL_VERSION}/lib -L/usr/local/opt/openssl@3/lib"
export CPPFLAGS="-I$(pwd)/.cache/curl-${CURL_VERSION}/include -I/usr/local/opt/openssl@3/include"

pip install --no-cache-dir --compile --install-option="--with-openssl" "pycurl==${PYCURL_VERSION}"

# Cleaning up
rm ".cache/curl-${CURL_VERSION}.tar.bz2"
rm -rf ".cache/curl-${CURL_VERSION}"

python -c "import pycurl" && echo "Success"
